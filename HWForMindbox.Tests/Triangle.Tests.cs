﻿using FiguresLibrary;
using System;
using Xunit;

namespace HWForMindbox.Tests
{
    public class TriangleTests
    {
        [Theory]
        [InlineData(0, 1, 1)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 0)]
        public void Triangle_CreateTrangleWithInvalidArgument_ThrowsArgumentException(int line1, int line2, int line3)
        {
            Assert.Throws<ArgumentException>(() => new Triangle(line1, line2, line3));
        }

        [Fact]
        public void Triangle_CreateCircleWithValidArgument_RadiusValue6()
        {
            
            int line1 = 1, line2 = 2, line3 = 3;

            var triangle = new Triangle(line1,line2,line3);

            Assert.Equal(triangle.Line1, line1);
            Assert.Equal(triangle.Line2, line2);
            Assert.Equal(triangle.Line3, line3);
        }

        [Theory]
        [InlineData(1, 4, 4, 1.984313483298443)]
        [InlineData(6, 5, 4, 9.921567416492215)]
        public void Triangle_GetArea_ValidValueArea(int line1, int line2, int line3, double validArea)
        {
            var triangle = new Triangle(line1, line2, line3);

            var area = triangle.GetArea();

            Assert.Equal(area, validArea);
        }

        [Theory]
        [InlineData(6, 8, 10, true)]
        [InlineData(6, 5, 4, false)]
        public void Triangle_IsRightTriangle_CheckTriangleIsRightTriangle(int line1, int line2, int line3, bool validResult)
        {
            var triangle = new Triangle(line1, line2, line3);

            var result = triangle.IsRightTriangle();

            Assert.Equal(result, validResult);
        }
    }
}
