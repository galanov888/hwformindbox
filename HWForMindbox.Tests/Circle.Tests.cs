using FiguresLibrary;
using System;
using Xunit;

namespace HWForMindbox.Tests
{
    public class CircleTests
    {
        [Fact]
        public void Circle_CreateCircleWithInvalidArgument_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new Circle(-1));
        }

        [Fact]
        public void Circle_CreateCircleWithValidArgument_RadiusValue6()
        {
            int value = 6;

            var circle = new Circle(value);

            Assert.Equal(circle.Radius, value);
        }

        [Theory]
        [InlineData(1, 3.141592653589793)]
        [InlineData(3, 28.274333882308138)]
        public void Circle_GetArea_ValidValueArea(int radius, double validArea)
        {
            var circle = new Circle(radius);

            var area = circle.GetArea();

            Assert.Equal(validArea, area);
        }
    }
}
