﻿using System;
using FiguresLibrary;

namespace ExampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Example();
            Console.WriteLine("Hello World!");
        }

        static void Example()
        {
            IFigure[] figures =  {
                new Circle(5),
                new Circle (10),
                new Triangle(1, 4, 4),
                new Triangle(6, 8, 10)
            };

            foreach (var figure in figures)
            {
                Console.WriteLine($"S = { figure.GetArea()}");
                if (figure is Triangle triangle)
                {
                    Console.WriteLine($"Треугольник является прямоугольным: {triangle.IsRightTriangle()}");
                }
            }
        }
    }
}
