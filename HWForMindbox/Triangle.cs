﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace FiguresLibrary
{
    public class Triangle : IFigure
    {
        private int _line1,
            _line2,
            _line3;

        public int Line1
        {
            get
            {
                return _line1;
            }
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Вводимое значение должно быть больше 0");
                }
                _line1 = value;
            }
        }
        public int Line2
        {
            get
            {
                return _line2;
            }
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Вводимое значение должно быть больше 0");
                }
                _line2 = value;
            }
        }

        public int Line3
        {
            get
            {
                return _line3;
            }
            private set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Вводимое значение должно быть больше 0");
                }
                _line3 = value;
            }
        }


        public Triangle(int line1 , int line2, int line3)
        {
            Line1 = line1;
            Line2 = line2;
            Line3 = line3;
        }

        public int GetPerimeter()
        {
            return (_line1 + _line2 + _line3);
        }

        public double GetArea()
        {
            double p = GetPerimeter() / 2.0;
            return Math.Sqrt(p * (p - _line1) * (p - _line2) * (p - _line3));
        }

        public bool IsRightTriangle()
        {
            var maxValue = getMaxValue();
            var hasEqualsValue = getLines().Count(v => v == maxValue) > 1;
            if (hasEqualsValue)
            {
                return false;
            }

            var c = Math.Pow(maxValue, 2);

            var a_b = getLines().Where(v => v != maxValue)
                .Select(v=> Math.Pow(v,2))
                .Sum();

            return c == a_b;
        }

        private IEnumerable<int> getLines()
        {
            return (new int[3] { _line1, _line2, _line3 });
        }

        private int getMaxValue()
        {
            return getLines().Max();
        }
    }
}
