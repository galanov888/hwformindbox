﻿using System; 

namespace FiguresLibrary
{
    public class Circle : IFigure
    {
        private int _radius;

        public int Radius {
            get
            {
                return _radius;
            }
        private set
            {
                if (value <1)
                {
                    throw new ArgumentException("Вводимое значение должно быть больше 0");
                }
                _radius = value;
            }
        }

        public Circle(int radius)
        {
            Radius = radius;
        }

        public double GetArea()
        {
            return Math.Pow(_radius, 2) * Math.PI;
        }
    }
}
