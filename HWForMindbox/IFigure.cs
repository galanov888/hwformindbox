﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresLibrary
{
    public interface IFigure
    {
        double GetArea();
    }
}
